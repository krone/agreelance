from django.contrib.auth.models import User
from django.core.management import call_command
from django.core import mail
from django.test import TestCase
from django.urls import reverse
from projects.models import TaskOffer as OfferModel
import sys


# Create your tests here.


class NewProjectTestCase(TestCase):
    def setUp(self):
        call_command('loaddata', 'seed_test.json', verbosity=0)
        self.url = reverse('projects')
        self.url += 'new/'
        user = User.objects.get_by_natural_key('admin')
        self.client.force_login(user)

    def test_new_project(self):
        mail.outbox = []
        self.client.post(self.url, {'title': 'title', 'description': 'description', 'category_id': 1,
                                    'task_title': 'task title', 'task_budget': 1, 'task_description': 'task desc'})
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'New Project: title')



