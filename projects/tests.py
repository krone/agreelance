from django.core.management import call_command
from django.test import TestCase, Client
from django.urls import reverse

# Create your tests here.


class ProjectViewTestCase(TestCase):
    def setUp(self):
        call_command('loaddata', 'seed_test.json', verbosity=0)



    def test_budget_filter_empty(self):
        url = reverse('projects')
        url += '?budget_lower_bound_input=&budget_upper_bound_input=&filter_button=Apply+Filter'
        resp = self.client.get(url)
        projects = list(resp.context[-1]['projects'])
        self.assertEqual(4, len(projects))

    def test_budget_filter_lower_zero(self):
        url = reverse('projects')
        url += '?budget_lower_bound_input=&budget_upper_bound_input=&filter_button=Apply+Filter'
        resp = self.client.get(url)
        projects = list(resp.context[-1]['projects'])
        self.assertEqual(4, len(projects))

    def test_budget_filter_borders_included(self):
        url = reverse('projects')
        url += '?budget_lower_bound_input=10&budget_upper_bound_input=40&filter_button=Apply+Filter'
        resp = self.client.get(url)
        projects = list(resp.context[-1]['projects'])
        self.assertEqual(3, len(projects))
        self.assertSetEqual(set(map(lambda x: x.title, iter(projects))), {'Test Project 3', 'Test Project 1', 'Test Project 2'})

    def test_budget_filter_borders_excluded(self):
        url = reverse('projects')
        url += '?budget_lower_bound_input=11&budget_upper_bound_input=39&filter_button=Apply+Filter'
        resp = self.client.get(url)
        projects = list(resp.context[-1]['projects'])
        self.assertEqual(1, len(projects))
        self.assertEqual(projects[0].title, 'Test Project 1')

    def test_budget_filter_non_number_input(self):
        url = reverse('projects')
        url += '?budget_lower_bound_input=asdf&budget_upper_bound_input=&filter_button=Apply+Filter'
        resp = self.client.get(url)
        projects = list(resp.context[-1]['projects'])
        self.assertEqual(4, len(projects))
        self.assertEqual('Please enter only numbers to the budget filters.', resp.context[-1]['budget_error_message'])

    def test_search_empty(self):
        url = reverse('projects')
        url += '?search_string_input=&filter_button=Apply+Filter'
        resp = self.client.get(url)
        projects = list(resp.context[-1]['projects'])
        self.assertEqual(4, len(projects))

    def test_search_title(self):
        url = reverse('projects')
        url += '?search_string_input=Project+1&filter_button=Apply+Filter'
        resp = self.client.get(url)
        projects = list(resp.context[-1]['projects'])
        self.assertEqual(1, len(projects))
        self.assertEqual(projects[0].title, 'Test Project 1')

    def test_search_description(self):
        url = reverse('projects')
        url += '?search_string_input=Description+3&filter_button=Apply+Filter'
        resp = self.client.get(url)
        projects = list(resp.context[-1]['projects'])
        self.assertEqual(2, len(projects))
        self.assertSetEqual(set(map(lambda x: x.title, iter(projects))), {'Test Project 3', 'Test Project 4'})

    def test_search_user(self):
        url = reverse('projects')
        url += '?search_string_input=adm&filter_button=Apply+Filter'
        resp = self.client.get(url)
        projects = list(resp.context[-1]['projects'])
        self.assertEqual(4, len(projects))

    def test_search_no_match(self):
        url = reverse('projects')
        url += '?search_string_input=asdf&filter_button=Apply+Filter'
        resp = self.client.get(url)
        projects = list(resp.context[-1]['projects'])
        self.assertEqual(0, len(projects))
