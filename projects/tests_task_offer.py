from django.contrib.auth.models import User
from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse
from projects.models import TaskOffer as OfferModel
from unittest import skip
import sys


# Create your tests here.


class TaskOfferTestCase(TestCase):
    def setUp(self):
        call_command('loaddata', 'seed_test.json', verbosity=0)
        self.url = reverse('projects')
        self.url += '1/'

    def test_valid_offer(self):
        user = User.objects.get_by_natural_key('joe')
        self.client.force_login(user)
        self.client.post(self.url, {'title': 'offerTitle', 'description': 'offerDescription', 'price': 35, 'taskvalue': 2, 'offer_submit': ''})
        all_offers = list(OfferModel.objects.all())
        self.assertEqual(3, len(all_offers))
        offer = list(filter(lambda x: x.task.id == 2, iter(all_offers)))[0]
        self.check_offer(offer, user_id=user.id, offer_price=35)

    def test_offer_without_user(self):
        self.assertRaisesMessage(AttributeError, '\'AnonymousUser\' object has no attribute \'profile\'',
                                 self.client.post, self.url, {'title': 'offerTitle', 'description': 'offerDescription', 'price': 35, 'taskvalue': 2, 'offer_submit': ''})
        all_offers = list(OfferModel.objects.all())
        self.assertEqual(2, len(all_offers))

    @skip('negative offers are possible right now')
    def test_negative_offer(self):
        user = User.objects.get_by_natural_key('joe')
        self.client.force_login(user)
        self.client.post(self.url, {'title': 'offerTitle', 'description': 'offerDescription', 'price': -1, 'taskvalue': 2, 'offer_submit': ''})
        all_offers = list(OfferModel.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(2, len(all_offers))

    def test_zero_offer(self):
        user = User.objects.get_by_natural_key('joe')
        self.client.force_login(user)
        self.client.post(self.url,
                         {'title': 'offerTitle', 'description': 'offerDescription', 'price': 0, 'taskvalue': 2,
                          'offer_submit': ''})
        all_offers = list(OfferModel.objects.all())
        self.assertEqual(3, len(all_offers))
        offer = list(filter(lambda x: x.task.id == 2, iter(all_offers)))[0]
        self.check_offer(offer, user_id=user.id, offer_price=0)

    def test_high_offer(self):
        user = User.objects.get_by_natural_key('joe')
        self.client.force_login(user)
        price = sys.maxsize
        self.client.post(self.url,
                         {'title': 'offerTitle', 'description': 'offerDescription', 'price': price, 'taskvalue': 2,
                          'offer_submit': ''})
        all_offers = list(OfferModel.objects.all())
        self.assertEqual(3, len(all_offers))
        offer = list(filter(lambda x: x.task.id == 2, iter(all_offers)))[0]
        self.check_offer(offer, user_id=user.id, offer_price=price)


    def test_price_string_offer(self):
        user = User.objects.get_by_natural_key('joe')
        self.client.force_login(user)
        self.client.post(self.url, {'title': 'offerTitle', 'description': 'offerDescription', 'price': '', 'taskvalue': 2, 'offer_submit': ''})
        all_offers = list(OfferModel.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(2, len(all_offers))

    def test_200_char_title_offer(self):
        user = User.objects.get_by_natural_key('joe')
        self.client.force_login(user)
        title = 'x' * 200
        self.client.post(self.url, {'title': title, 'description': 'offerDescription', 'price': 35, 'taskvalue': 2, 'offer_submit': ''})
        all_offers = list(OfferModel.objects.all())
        self.assertEqual(3, len(all_offers))
        offer = list(filter(lambda x: x.task.id == 2, iter(all_offers)))[0]
        self.check_offer(offer, user_id=user.id, offer_price=35, offer_title=title)

    def test_201_char_title_offer(self):
        user = User.objects.get_by_natural_key('joe')
        self.client.force_login(user)
        title = 'x' * 201
        self.client.post(self.url, {'title': title, 'description': 'offerDescription', 'price': 35, 'taskvalue': 2,
                                    'offer_submit': ''})
        all_offers = list(OfferModel.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(2, len(all_offers))

    # The requirements for this are not available, but long descriptions (512 chars) are not supported
    def test_long_desc_offer(self):
        user = User.objects.get_by_natural_key('joe')
        self.client.force_login(user)
        desc = 'x' * 2**9
        self.client.post(self.url, {'title': 'offerTitle', 'description': desc, 'price': 35, 'taskvalue': 2, 'offer_submit': ''})
        all_offers = list(OfferModel.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(2, len(all_offers))

    def check_offer(self, offer, user_id, task_id=2, offer_title='offerTitle', offer_desc='offerDescription', offer_price=0):
        self.assertEqual(offer_title, offer.title)
        self.assertEqual(offer_desc, offer.description)
        self.assertEqual(offer_price, offer.price)
        self.assertEqual(task_id, offer.task.id)
        self.assertEqual(user_id, offer.offerer.id)



