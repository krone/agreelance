from django.contrib.auth.models import User
from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse
from projects.models import TaskOffer as OfferModel
import sys


# Create your tests here.


class OfferResponseTestCase(TestCase):
    def setUp(self):
        call_command('loaddata', 'seed_test.json', verbosity=0)
        self.url = reverse('projects')
        self.url += '1/'
        user = User.objects.get_by_natural_key('admin')
        self.client.force_login(user)

# output domain is the task status, either 'accepted', 'pending 'and 'declined', and the feedback string.
    def test_accept(self):
        self.client.post(self.url, {'status': 'a', 'feedback': 'feedbackString', 'taskofferid': 1, 'offer_response': ''})
        all_offers = list(OfferModel.objects.all())
        self.assertEqual(2, len(all_offers))
        offer = self.get_offer()
        self.assertEqual('a', offer.status)
        self.assertEqual('feedbackString', offer.feedback)

    def test_pending(self):
        self.client.post(self.url, {'status': 'p', 'feedback': 'feedbackString', 'taskofferid': 1, 'offer_response': ''})
        all_offers = list(OfferModel.objects.all())
        self.assertEqual(2, len(all_offers))
        offer = self.get_offer()
        self.assertEqual('p', offer.status)
        self.assertEqual('feedbackString', offer.feedback)

    def test_declined(self):
        self.client.post(self.url, {'status': 'd', 'feedback': 'feedbackString', 'taskofferid': 1, 'offer_response': ''})
        all_offers = list(OfferModel.objects.all())
        self.assertEqual(2, len(all_offers))
        offer = self.get_offer()
        self.assertEqual('d', offer.status)
        self.assertEqual('feedbackString', offer.feedback)

    def test_random_string_state(self):
        resp = self.client.post(self.url, {'status': 'r', 'feedback': 'feedbackString', 'taskofferid': 1, 'offer_response': ''})
        self.assertEqual(200, resp.status_code)
        all_offers = list(OfferModel.objects.all())
        self.assertEqual(2, len(all_offers))
        offer = self.get_offer()
        self.assertEqual('p', offer.status)
        self.assertEqual('', offer.feedback)

    def test_no_feedback(self):
        resp = self.client.post(self.url, {'status': 'a', 'feedback': '', 'taskofferid': 1, 'offer_response': ''})
        self.assertEqual(200, resp.status_code)
        all_offers = list(OfferModel.objects.all())
        self.assertEqual(2, len(all_offers))
        offer = self.get_offer()
        self.assertEqual('p', offer.status)
        self.assertEqual('', offer.feedback)

    def get_offer(self, id=1):
        all_offers=OfferModel.objects.all()
        offer=list(filter(lambda x: x.task.id == 1, iter(all_offers)))[0]
        return offer
