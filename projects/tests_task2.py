from django.core.management import call_command
from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from projects.models import TaskOffer,Project, Task
from projects.views import get_user_task_permissions



class TaskTwoTestCase(TestCase):
    def setUp(self):
        call_command('loaddata', 'seed_test.json', verbosity=0)
        self.url = reverse('projects')
        self.url += '1/'

    def test_send_offer_response(self):
        user= User.objects.get_by_natural_key('admin')
        self.client.force_login(user)
        self.client.post(self.url,{'status':'a','feedback':'feedback_string','offer_response':'','taskofferid':1})
        all_offers=TaskOffer.objects.all()
        offer=list(filter(lambda x: x.task.id == 1, iter(all_offers)))[0]
        self.assertEqual('a', offer.status)
        self.assertEqual('feedback_string',offer.feedback)

    def test_change_status(self):
        user = User.objects.get_by_natural_key('admin')
        self.client.force_login(user)
        self.client.post(self.url,{'status': 'f',  'status_change': ''})
        project=list(filter(lambda x: x.id == 1, iter(Project.objects.all())))[0]
        self.assertEqual('f',project.status)

    def test_offer_submit(self):
        user=User.objects.get_by_natural_key('joe')
        self.client.force_login(user)
        self.client.post(self.url,{'offer_submit':'','taskvalue':'2','title':'title_str', 'description':'des_str', 'price':42})
        all_offers = list(TaskOffer.objects.all())
        self.assertEqual(3, len(all_offers))
        offer = list(filter(lambda x: x.task.id == 2, iter(all_offers)))[0]
        self.assertEqual('title_str',offer.title)
        self.assertEqual('des_str',offer.description)
        self.assertEqual(42,offer.price)


    def test_permissions_project_owner(self):
        user=User.objects.get_by_natural_key('admin')
        task=list(filter(lambda x: x.id == 1, iter(Task.objects.all())))[0]
        permission=get_user_task_permissions(user,task)
        self.assertTrue(permission['write'])
        self.assertTrue(permission['read'])
        self.assertTrue(permission['modify'])
        self.assertTrue(permission['owner'])
        self.assertTrue(permission['upload'])

    def test_permissions_accepted(self):
        user=User.objects.get_by_natural_key('joe')
        task=list(filter(lambda x: x.id == 3, iter(Task.objects.all())))[0]
        permission=get_user_task_permissions(user,task)
        self.assertTrue(permission['write'])
        self.assertTrue(permission['read'])
        self.assertTrue(permission['modify'])
        self.assertFalse(permission['owner'])
        self.assertTrue(permission['upload'])

    def test_user_permission(self):
        user= User.objects.get_by_natural_key('harrypotter')
        task = list(filter(lambda x: x.id == 3, iter(Task.objects.all())))[0]
        permission = get_user_task_permissions(user, task)
        self.assertFalse(permission['write'])
        self.assertFalse(permission['read'])
        self.assertFalse(permission['modify'])
        self.assertFalse(permission['owner'])
        self.assertFalse(permission['upload'])





