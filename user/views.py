from django.http import HttpResponse
from projects.models import ProjectCategory
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

from .forms import SignUpForm
from .models import Address


def index(request):
    return render(request, 'base.html')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()

            user.profile.company = form.cleaned_data.get('company')
            user.profile.phone_number = form.cleaned_data.get('phone_number')
            address = Address()
            address.country = form.cleaned_data.get('country')
            address.state = form.cleaned_data.get('state')
            address.city = form.cleaned_data.get('city')
            address.postal_code = form.cleaned_data.get('postal_code')
            address.street_address = form.cleaned_data.get('street_address')
            address.save()

            user.profile.address = address

            user.is_active = False
            user.profile.categories.add(*form.cleaned_data['categories'])
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            from django.contrib import messages
            messages.success(request, 'Your account has been created and is awaiting verification.')
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'user/signup.html', {'form': form})


#Instead of returning a 401 Unauthorized, the user is forwarded to le login page.
@login_required
def profile(request):
    user = request.user
    return render(request, 'user/profile.html', {'user': user})
