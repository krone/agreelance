from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

# Create your tests here.


class SignUpTestCase(TestCase):
    def setUp(self):
        call_command('loaddata', 'seed_test.json', verbosity=0)
        self.url = reverse('signup')
        self.data = {
            'username': 'test_user',
            'password1': '12qwertz23',
            'password2': '12qwertz23',
            'first_name': 'fname',
            'last_name': 'lname',
            'company': 'company',
            'email': 'mail@mail.no',
            'email_confirmation': 'mail@mail.no',
            'phone_number': '123456',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': 'postal_code',
            'street_address': 'street',
            'categories': [1]
        }

        self.long_data = {
            'username': 'x' * 150,
            'password1': '12qwertz23',
            'password2': '12qwertz23',
            'first_name': 'x' * 30,
            'last_name': 'x' * 30,
            'company': 'x' * 30,
            'email': 'x' * 246 + '@' + 'mail.no',
            'email_confirmation': 'x' * 246 + '@' + 'mail.no',
            'phone_number': 'x' * 50,
            'country': 'x' * 50,
            'state': 'x' * 50,
            'city': 'x' * 50,
            'postal_code': 'x' * 50,
            'street_address': 'x' * 50,
            'categories': [1, 2, 3]
        }

    def test_sign_up_valid_cleaning(self):
        self.client.post(self.url, self.data)
        all_users = list(User.objects.all())
        self.assertEqual(4, len(all_users))
        user = list(filter(lambda x: x.username == 'test_user', iter(all_users)))[0]
        self.check_user(user)

    def test_painting(self):
        self.data['categories'] = [2]
        self.client.post(self.url, self.data)
        all_users = list(User.objects.all())
        self.assertEqual(4, len(all_users))
        user = list(filter(lambda x: x.username == 'test_user', iter(all_users)))[0]
        self.check_user(user, categories={'Painting'})

    def test_gardening(self):
        self.data['categories'] = [3]
        self.client.post(self.url, self.data)
        all_users = list(User.objects.all())
        self.assertEqual(4, len(all_users))
        user = list(filter(lambda x: x.username == 'test_user', iter(all_users)))[0]
        self.check_user(user, categories={'Gardening'})

    def test_cleaning_painting(self):
        self.data['categories'] = [1, 2]
        self.client.post(self.url, self.data)
        all_users = list(User.objects.all())
        self.assertEqual(4, len(all_users))
        user = list(filter(lambda x: x.username == 'test_user', iter(all_users)))[0]
        self.check_user(user, categories={'Cleaning', 'Painting'})

    def test_cleaning_gardening(self):
        self.data['categories'] = [1, 3]
        self.client.post(self.url, self.data)
        all_users = list(User.objects.all())
        self.assertEqual(4, len(all_users))
        user = list(filter(lambda x: x.username == 'test_user', iter(all_users)))[0]
        self.check_user(user, categories={'Cleaning', 'Gardening'})

    def test_painting_gardening(self):
        self.data['categories'] = [2, 3]
        self.client.post(self.url, self.data)
        all_users = list(User.objects.all())
        self.assertEqual(4, len(all_users))
        user = list(filter(lambda x: x.username == 'test_user', iter(all_users)))[0]
        self.check_user(user, categories={'Painting', 'Gardening'})

    def test_all_categories(self):
        self.data['categories'] = [3, 2, 1]
        self.client.post(self.url, self.data)
        all_users = list(User.objects.all())
        self.assertEqual(4, len(all_users))
        user = list(filter(lambda x: x.username == 'test_user', iter(all_users)))[0]
        self.check_user(user, categories={'Cleaning', 'Painting', 'Gardening'})

    # a user has to have at least one category
    def test_no_categories(self):
        self.data['categories'] = []
        self.client.post(self.url, self.data)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_long_input(self):
        self.client.post(self.url, self.long_data)
        all_users = list(User.objects.all())
        self.assertEqual(4, len(all_users))
        user = list(filter(lambda x: x.username == 'x' * 150, iter(all_users)))[0]
        x_30 = 'x' * 30
        x_50 = 'x' * 50
        self.check_user(user, 'x'*150, x_30, x_30, x_30, x_50, x_50, x_50, x_50, x_50, x_50, {'Cleaning', 'Gardening', 'Painting'}, 'x' * 246 + '@' + 'mail.no')

    def test_username_empty(self):
        self.data['username'] = ''
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_first_name_empty(self):
        self.data['first_name'] = ''
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_last_name_empty(self):
        self.data['last_name'] = ''
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    # passwords are not restricted in length, we therefore only test for empty passwords
    def test_password_empty(self):
        self.data['password1'] = ''
        self.data['password2'] = ''
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_email_empty(self):
        self.data['email'] = ''
        self.data['email_confirmation'] = ''
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_company_empty(self):
        self.data['company'] = ''
        resp = self.client.post(self.url, self.data)
        all_users = list(User.objects.all())
        self.assertEqual(4, len(all_users))
        user = list(filter(lambda x: x.username == 'test_user', iter(all_users)))[0]
        self.check_user(user, company='')

    def test_phone_empty(self):
        self.data['phone_number'] = ''
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_country_empty(self):
        self.data['country'] = ''
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_state_empty(self):
        self.data['state'] = ''
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_city_empty(self):
        self.data['city'] = ''
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_postal_code_empty(self):
        self.data['postal_code'] = ''
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_username_long(self):
        self.data['username'] = 'x'*151
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_first_name_long(self):
        self.data['first_name'] = 'x' * 31
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_last_name_long(self):
        self.data['last_name'] = 'x' * 31
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_company_long(self):
        self.data['company'] = 'x' * 31
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_email_long(self):
        mail = 'x' * 247 + '@' + 'mail.no'
        self.data['email'] = mail
        self.data['email_confirmation'] = mail
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_phone_long(self):
        self.data['phone_number'] = 'x' * 51
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_country_long(self):
        self.data['country'] = 'x' * 51
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_state_long(self):
        self.data['state'] = 'x' * 51
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_city_long(self):
        self.data['city'] = 'x' * 51
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_postal_code_long(self):
        self.data['postal_code'] = 'x' * 51
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    def test_invalid_categories_long(self):
        self.data['categories'] = [0]
        resp = self.client.post(self.url, self.data)
        self.assertEqual(200, resp.status_code)
        all_users = list(User.objects.all())
        # invalid forms are not processed, but no exception is raised
        self.assertEqual(3, len(all_users))

    # passwords are stored as salted hashes, testing for equality here does not make too much sense.
    def check_user(self, user, username='test_user', first_name='fname', last_name='lname',
                   company='company', phone='123456', country='country', state='state',
                   city='city', postal_code='postal_code', street_address='street', categories={'Cleaning'}, email='mail@mail.no'):
        self.assertEqual(username, user.username)
        self.assertEqual(first_name, user.first_name)
        self.assertEqual(last_name, user.last_name)
        self.assertEqual(company, user.profile.company)
        self.assertEqual(email, user.email)
        self.assertEqual(phone, user.profile.phone_number)
        self.assertEqual(country, user.profile.address.country)
        self.assertEqual(state, user.profile.address.state)
        self.assertEqual(city, user.profile.address.city)
        self.assertEqual(postal_code, user.profile.address.postal_code)
        self.assertEqual(street_address, user.profile.address.street_address)
        self.assertSetEqual(categories, set(map(lambda x: x.name, iter(user.profile.categories.all()))))
