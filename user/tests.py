from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

# Create your tests here.


class UserViewTestCase(TestCase):
    def setUp(self):
        call_command('loaddata', 'seed_test.json', verbosity=0)

    def test_profile_page(self):
        logged_in_user = User.objects.get_by_natural_key('joe')
        self.client.force_login(logged_in_user)
        url = reverse('profile')
        resp = self.client.get(url)
        user = resp.context[-1]['user']
        self.assertEqual(200, resp.status_code)
        self.assertEqual('joe', user.username)
        self.assertEqual('Joe', user.first_name)
        self.assertEqual('Dough', user.last_name)
        self.assertEqual('Dough gardens', user.profile.company)
        self.assertEqual('jd@garden.example.com', user.email)
        self.assertEqual('123456', user.profile.phone_number)
        self.assertEqual('Norway', user.profile.address.country)
        self.assertEqual('Trøndelag', user.profile.address.state)
        self.assertEqual('Trondheim', user.profile.address.city)
        self.assertEqual('7050', user.profile.address.postal_code)
        self.assertEqual('Street 5', user.profile.address.street_address)
        categories = user.profile.categories.all()
        self.assertEqual(2, len(categories))
        categories = set(map(lambda x: x.name, iter(categories)))
        self.assertSetEqual(categories, {'Cleaning', 'Gardening'})

    def test_profile_page_unauthorized(self):
        url = reverse('profile')
        resp = self.client.get(url)
        # The user is forwarded to the login page and after the login to the profile page
        self.assertEqual(302, resp.status_code)
        print('/user/login/?next=/user/profile/', resp['Location'])
